<?php

namespace Database\Models;

use Illuminate\Database\Eloquent\Model;


class OfferTranslation extends Model
{
	protected $fillable = [
		'offer_id',
		'language',
		'translation',
	];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $hidden = [
		'',
	];
}

