<?php

namespace Database\Models;

use Illuminate\Database\Eloquent\Model;


class Contact extends Model
{
	protected $fillable = [
		'company_id',
		'email',
		'status'
	];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $hidden = [
		'',
	];

	public function newsletters()
	{
		$this->hasMany(Newsletter::class);
	}

	public function gdprTransactions()
	{
		$this->hasMany(GdprTransaction::class);
	}

	public function reservations()
	{
		$this->hasMany(Reservation::class);
	}
}

