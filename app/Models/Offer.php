<?php


namespace Database\Models;


use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
	protected $fillable = [
		'name',
		'status',
		'image',
		'redirect_url',
	];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $hidden = [
		'',
	];

	public function translations()
	{
		$this->hasMany(OfferTranslation::class);
	}

	public function seasons()
	{
		$this->hasMany(OfferSeason::class);
	}

	public function affiliates()
	{
		return $this->belongsToMany(Affiliate::class)->using(AffiliateOffer::class);
	}

	public function extras()
	{
		return $this->belongsToMany(Extra::class)->using(OfferExtra::class);
	}
}
