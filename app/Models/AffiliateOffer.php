<?php


namespace Database\Models;


use Illuminate\Database\Eloquent\Model;

class AffiliateOffer extends Model
{
	protected $fillable = [
		'offer_id',
		'company_id',
		'shares'
	];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $hidden = [
		'',
	];
}
