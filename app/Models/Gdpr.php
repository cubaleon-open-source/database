<?php


namespace Database\Models;


use Illuminate\Database\Eloquent\Model;

class Gdpr extends Model
{
	protected $fillable = [
		'location',
	];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $hidden = [
		'',
	];

	public function gdprTransactions()
	{
		$this->hasMany(GdprTransaction::class);
	}
}
