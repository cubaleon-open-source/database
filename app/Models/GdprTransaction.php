<?php


namespace Database\Models;


use Illuminate\Database\Eloquent\Model;

class GdprTransaction extends Model
{
	protected $fillable = [
		'company_id',
		'contact_id',
		'gdpr_id',
	];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $hidden = [
		'',
	];
}
