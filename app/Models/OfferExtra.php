<?php

namespace Database\Models;

use Illuminate\Database\Eloquent\Model;


class OfferExtra extends Model
{
	protected $fillable = [
		'offer_id',
		'extra_id',
	];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $hidden = [
		'',
	];
}

