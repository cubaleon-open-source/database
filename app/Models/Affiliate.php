<?php

namespace Database\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Laravel\Passport\HasApiTokens;

/**
 * @property int       $id
 * @property int       $settings_id
 * @property string    $file_name
 * @property \DateTime $send_at
 */
class Affiliate extends Model implements AuthorizableContract, AuthenticatableContract
{
	use Authenticatable, Authorizable, HasApiTokens;


	protected $fillable = [
		'company_name',
		'domain',
		'status',
		'email',
		'password'
	];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
	];

	public function contacts()
	{
		return $this->hasMany(Contact::class);
	}

	public function gdprTransactions()
	{
		$this->hasMany(GdprTransaction::class);
	}

	public function reservations()
	{
		$this->hasMany(Reservation::class);
	}

	public function offers()
	{
		return $this->belongsToMany(Offer::class)->using(AffiliateOffer::class);
	}
}

