<?php

namespace Database\Models;

use Illuminate\Database\Eloquent\Model;


class OfferSeason extends Model
{
	protected $fillable = [
		'offer_id',
		'high_season',
		'low_season',
		'price',
	];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $hidden = [
		'',
	];
}

