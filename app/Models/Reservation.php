<?php

namespace Database\Models;

use Illuminate\Database\Eloquent\Model;


class Reservation extends Model
{
	protected $fillable = [
		'contact_id',
		'company_id',
		'details',
	];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $hidden = [
		'',
	];
}

