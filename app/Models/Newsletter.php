<?php


namespace Database\Models;


use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
	protected $fillable = [
		'contact_id',
		'status',
	];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $hidden = [
		'',
	];
}
