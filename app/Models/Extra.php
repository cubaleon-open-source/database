<?php


namespace Database\Models;


use Illuminate\Database\Eloquent\Model;

class Extra extends Model
{
	protected $fillable = [
		'name',
		'price',
	];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $hidden = [
		'',
	];

    public function offers()
    {
        return $this->belongsToMany(Offer::class)->using(OfferExtra::class);
    }
}
