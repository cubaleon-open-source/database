<?php


namespace Database\Repositories;


use Database\Models\Affiliate;

/**
 * Class AffiliateRepository
 *
 * @package Database\Repositories
 */
class AffiliateRepository
{
	public function createAffiliate(
		string $companyName,
		string $domain,
		string $email,
		string $password
	): Affiliate
	{
		$affiliate = Affiliate::create([
			'company_name' => $companyName,
			'domain'       => $domain,
			'email'        => $email,
			'password'     => $password,
			'status'       => false,
		]);

		return $affiliate;
	}
}
