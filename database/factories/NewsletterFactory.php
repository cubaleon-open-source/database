<?php

use Database\Models\Newsletter;

$factory->define(Newsletter::class, function (Faker\Generator $faker) {
    return [
        'contact_id' => 1,
        'status'     => $faker->boolean(50),
    ];
});



