<?php

use Database\Models\OfferTranslation;

$factory->define(OfferTranslation::class, function (Faker\Generator $faker) {
    return [
        'offer_id'    => 1,
        'language'    => $faker->languageCode,
        'translation' => '{"example": "example"}',
    ];
});



