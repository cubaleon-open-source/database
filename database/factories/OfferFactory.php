<?php

use Database\Models\Offer;

$factory->define(Offer::class, function (Faker\Generator $faker) {
    return [
        'name'         => $faker->name,
        'status'       => $faker->boolean(90),
        'image'        => $faker->address,
        'redirect_url' => $faker->address,
    ];
});



