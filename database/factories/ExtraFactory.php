<?php

use Database\Models\Extra;

$factory->define(Extra::class, function (Faker\Generator $faker) {
    return [
        'name'         => $faker->name,
        'price'       => $faker->numberBetween(50,1000),
    ];
});



