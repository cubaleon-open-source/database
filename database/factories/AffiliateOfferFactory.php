<?php

use Database\Models\Affiliate;

$factory->define(Affiliate::class, function (Faker\Generator $faker) {
    return [
        'affiliate_id' => 'Cubaleon',
        'domain'       => 'cubaleon.com',
        'status'       => true,
        'email'        => 'support@cubaleon.com',
        'password'     => \Illuminate\Support\Facades\Hash::make('pass'),
    ];
});



