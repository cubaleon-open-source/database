<?php

use Database\Models\OfferSeason;

$factory->define(OfferSeason::class, function (Faker\Generator $faker) {
    return [
        'offer_id'          => 1,
        'high_season_start' => $faker->date('Y-m-d'),
        'high_season_end'   => $faker->date('Y-m-d'),
        'low_season_start'  => $faker->date('Y-m-d'),
        'low_season_end'    => $faker->date('Y-m-d'),
        'price'             => $faker->numberBetween(250, 5000),
    ];
});



