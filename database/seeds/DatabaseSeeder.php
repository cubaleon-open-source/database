<?php

use Database\Models\Affiliate;
use Database\Models\Contact;
use Database\Models\Extra;
use Database\Models\Gdpr;
use Database\Models\Newsletter;
use Database\Models\Offer;
use Database\Models\OfferExtra;
use Database\Models\OfferSeason;
use Database\Models\OfferTranslation;
use Database\Models\Reservation;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** Create 1 companies */
        factory(Affiliate::class, 1)->create();

        /** Create 1 companies */
        factory(Contact::class, 1)->create();

        /** Create 1 reservation */
        factory(Reservation::class, 1)->create();

        /** Create 1 gdpr */
        factory(Gdpr::class, 1)->create();

        /** Create 1 gdpr transaction */
        factory(Gdpr::class, 1)->create();

        /** Create 1 newsletter */
        factory(Newsletter::class, 1)->create();

        /** Create 1 offer */
        factory(Offer::class, 1)->create();

        /** Create 1 offer season */
        factory(OfferSeason::class, 1)->create();

        /** Create 1 extra */
        factory(Extra::class, 1)->create();

        /** Create 1 offer extra */
        factory(OfferExtra::class, 1)->create();

        /** Create 1 offer translation */
        factory(OfferTranslation::class, 1)->create();
    }
}
