<?php


return [
    'defaults' => [
        'guard'     => 'api',
        'passwords' => 'affiliate',
    ],

    'guards' => [
        'api' => [
            'driver'   => 'passport',
            'provider' => 'affiliate',
        ],
    ],

    'providers' => [
        'affiliate' => [
            'driver' => 'eloquent',
            'model'  => \Database\Models\Affiliate::class,
        ],
    ],
];
