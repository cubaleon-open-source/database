# Cubaleon Database 

* This repo should be able to run all migrations for all envs and seeds
on the local env.

* use it via composer require: .....

* you just need to instantiate the repository and start using it. Do not
develop any model or repository in your project.

* If you need to change something related to the DB, *DO IT HERE*

# TODO
* Create a pipleine that integrate this with packagist.org for Cubaleon.

